package pl.sda;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static java.lang.Integer.valueOf;

public class Ksiegozbior {

    private Set<Ksiazka> ksiazki = new HashSet<>();

    public void dodajKsiazke(String tytul, String autor){
        this.ksiazki.add(new Ksiazka(tytul, autor));
        System.out.println("Dodano ksiazke");
    }

    public void wyswietlPoId(String id){
        int newId = valueOf(id);
        for (Ksiazka ksiazka : ksiazki){
            if (ksiazka.getId() == newId) {
                System.out.println(ksiazka);
                return;
            }
        }
    }
    public void wyswietlWszystkie(){
        for (Ksiazka ksiazka : ksiazki){
            System.out.println(ksiazka.toString());
        }
    }
    public void wyswietlPoAutorze(String autor){
        for (Ksiazka ksiazka : ksiazki){
            if (ksiazka.getAutor().equals(autor)){
                System.out.println(ksiazka);
            }
        }
    }
    public void wypozycz (String id, String odKiedy, String doKiedy, String przezKodo){
        int newId = Integer.valueOf(id);
        LocalDate newOdKiedy = LocalDate.parse(odKiedy);
        LocalDate newDoKiedy;
        if (doKiedy.equals("")){
            newDoKiedy = newOdKiedy.plusWeeks(2);
        }else {
            newDoKiedy = LocalDate.parse(doKiedy);
        }
        for (Ksiazka ksiazka : ksiazki){
            if (ksiazka.getId() == newId){
                if (ksiazka.czyWypozyczone()){
                    System.out.println("Ktos inny wypozyczyl ksiazke");
                    return;
                }
                if (ksiazka.czyUszkodzona()){
                    System.out.println("Nie mozna wypozyczyc, ksiazka uszkodzona");
                    return;
                }
                ksiazka.wypozycz(newOdKiedy, newDoKiedy, przezKodo);
                System.out.println(ksiazka);
            }
        }
    }
    public void zwroc (String id){
        int newId = Integer.valueOf(id);
        for (Ksiazka ksiazka : ksiazki){
            if (ksiazka.getId() == newId){
                ksiazka.zwrocono();
                System.out.println(ksiazka);
            }
        }
    }
    public void usunKsiazke(String id){
        int newId = Integer.valueOf(id);
        Iterator<Ksiazka> iterator = ksiazki.iterator();
        while (iterator.hasNext()){
            Ksiazka ksiazka = iterator.next();

            if (ksiazka.getId() == newId){
                if (!ksiazka.czyWypozyczone()){
                    iterator.remove();
                    System.out.println("usunieto ksiazke");
                }else {
                    System.out.println("nie mozna usunąc wypozyczonej ksiazki");
                }
            }
        }
    }
    public void ksiazkaUszkodzona(String id){
        int newId = Integer.valueOf(id);
        for (Ksiazka ksiazka: ksiazki){
            if (ksiazka.getId() == newId){
                ksiazka.uszkodzona();
                System.out.println(ksiazka);
            }
        }
    }
    public void dataZwrotu(String id){
        int newId = Integer.valueOf(id);
        for (Ksiazka ksiazka: ksiazki){
            if (ksiazka.getId() == newId){
                if (ksiazka.czyWypozyczone()){
                    System.out.println(ksiazka.getWypozyczonaDo());
                }else {
                    System.out.println("Książka nie jest wypożyczona");
                }
            }
        }
    }
    public void ileDniDoZwrotu(String id){
        int newId = Integer.valueOf(id);
        for (Ksiazka ksiazka: ksiazki){
            if (ksiazka.getId() == newId){
                long ileDniPomiedzy = ChronoUnit.DAYS.between(ksiazka.getWypozyczonaOd(),ksiazka.getWypozyczonaDo());
                System.out.println("Zostało " + ileDniPomiedzy + " dni do konca wypozyczenia" );
            }
        }
    }

}
