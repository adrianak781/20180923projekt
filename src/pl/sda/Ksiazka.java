package pl.sda;

import java.time.LocalDate;

public class Ksiazka {
    private static int sekwencjaId = 0;

    private int id;
    private String tytul;
    private String autor;
    private StatusKsiazki status;

    private LocalDate wypozyczonaOd;
    private LocalDate wypozyczonaDo;
    private String wypozyczonaPrzez;

    public Ksiazka(String tytul, String autor) {
        sekwencjaId++;
        this.id =sekwencjaId;
        this.tytul = tytul;
        this.autor = autor;
        this.status = StatusKsiazki.DOSTEPNA;
    }

    public void wypozycz(LocalDate odKiedy, LocalDate doKiedy, String przezKogo){
        this.wypozyczonaOd = odKiedy;
        this.wypozyczonaDo = doKiedy;
        this.wypozyczonaPrzez = przezKogo;
        this.status = StatusKsiazki.WYPOZYCZONA;
    }

    public void zwrocono(){
        this.wypozyczonaOd = null;
        this.wypozyczonaDo = null;
        this.wypozyczonaPrzez = null;
        this.status = StatusKsiazki.DOSTEPNA;
    }

    @Override
    public String toString() {
        return this.id + "|" + this.autor + "|" + this.tytul + "|" + this.status.name();
    }

    public int getId() {
        return id;
    }

    public String getAutor() {
        return autor;
    }

    public boolean czyWypozyczone(){
        return this.status.equals(StatusKsiazki.WYPOZYCZONA);
    }
    public void uszkodzona(){
        this.status = StatusKsiazki.USZKODZONA;
    }
    public boolean czyUszkodzona(){
        return this.status.equals(StatusKsiazki.USZKODZONA);
    }
    public LocalDate getWypozyczonaDo() {
        return wypozyczonaDo;
    }

    public LocalDate getWypozyczonaOd() {
        return wypozyczonaOd;
    }
}
