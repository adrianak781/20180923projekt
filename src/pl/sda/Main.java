package pl.sda;

import java.time.format.DateTimeParseException;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Ksiegozbior ksiegozbior = new Ksiegozbior();

        while (true) {
            try {
                System.out.println("\n" +
                        "Komenda: \n" +
                        "Dodaj ksiazke: D;<tytuł>;<autor> \n" +
                        "Zakoncz program: Z \n" +
                        "Wyswietl wszystkie: W\n" +
                        "Wyswietl z podanym id: WID;<id> \n" +
                        "Wyswietl ksiązki danego autora: WAUT;<autor>\n" +
                        "Wypozycz: WYP;<id>;<od_kiedy>;<do_kiedy>;<przez_kogo> \n" +
                        "Oddaj: ODD;<id> \n" +
                        "Usuń: U;<id>\n" +
                        "Uszkodzona: USZ;<id>\n" +
                        "Kiedy książka ma być zwrócona: KIEDY;<id>\n" +
                        "Ile dni zostało do zwrotu: ILE;<id>\n");
                String komenda = scanner.nextLine();
                komenda = komenda.toUpperCase();
                if ("Z".equals(komenda)) {
                    return;
                }
                String[] czesciKomendy = komenda.split(";");
                switch (czesciKomendy[0]) {
                    case "D":
                        ksiegozbior.dodajKsiazke(czesciKomendy[1], czesciKomendy[2]);
                        break;
                    case "W":
                        ksiegozbior.wyswietlWszystkie();
                        break;
                    case "WID":
                        ksiegozbior.wyswietlPoId(czesciKomendy[1]);
                        break;
                    case "WAUT":
                        ksiegozbior.wyswietlPoAutorze(czesciKomendy[1]);
                        break;
                    case "WYP":
                        ksiegozbior.wypozycz(czesciKomendy[1], czesciKomendy[2], czesciKomendy[3], czesciKomendy[4]);
                        break;
                    case "ODD":
                        ksiegozbior.zwroc(czesciKomendy[1]);
                        break;
                    case "U":
                        ksiegozbior.usunKsiazke(czesciKomendy[1]);
                        break;
                    case "UZS":
                        ksiegozbior.ksiazkaUszkodzona(czesciKomendy[1]);
                        break;
                    case "KIEDY":
                        ksiegozbior.dataZwrotu(czesciKomendy[1]);
                        break;
                    case "ILE":
                        ksiegozbior.ileDniDoZwrotu(czesciKomendy[1]);
                        break;
                    default:
                        System.out.println("Nieznana komenda");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Nieprawidłowa ilość parametrow, sprobuj jeszcze raz.");
            } catch (DateTimeParseException e) {
                System.out.println("Niewlasciwy format daty, spróbuj jeszcze raz.");
            }

        }

    }
}
